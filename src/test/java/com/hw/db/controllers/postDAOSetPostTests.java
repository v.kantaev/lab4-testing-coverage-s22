package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.sql.Timestamp;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class postDAOSetPostTests {
    private Post post;

    @BeforeEach
    void setUp() {
        post = new Post();
    }

    void setUpGetPost(JdbcTemplate mockJdbc, Post post) {
        Mockito.when(mockJdbc.queryForObject(
                Mockito.any(String.class),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt()))
                .thenReturn(post);
    }

    @Test
    @DisplayName("SQL query includes author, message, and timestamp changes.")
    void changeAuthorMessageAndTimestamp() {
        post.setAuthor("author");
        post.setMessage("message");
        post.setCreated(new Timestamp(0));

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post toch = new Post();
        toch.setAuthor("a");
        toch.setMessage("m");
        toch.setCreated(new Timestamp(1));
        setUpGetPost(mockJdbc, toch);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc).update(Mockito.eq(
                "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq("author"), Mockito.eq("message"), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("SQL query includes message and timestamp changes.")
    void changeMessageAndTimestamp() {
        post.setMessage("message");
        post.setCreated(new Timestamp(0));

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post toch = new Post();
        toch.setMessage("m");
        toch.setCreated(new Timestamp(1));
        setUpGetPost(mockJdbc, toch);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc).update(Mockito.eq(
                "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq("message"), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("SQL query includes timestamp changes.")
    void changeTimestamp() {
        post.setCreated(new Timestamp(0));

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post toch = new Post();
        toch.setCreated(new Timestamp(1));
        setUpGetPost(mockJdbc, toch);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc).update(Mockito.eq(
                "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("SQL query includes no changes.")
    void changeNothing() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post toch = new Post();
        setUpGetPost(mockJdbc, toch);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }
}
